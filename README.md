# Xenopus_microbiomes

This is the code home for the manuscript:
"The role of family and environment in determining the skin microbiome of captive aquatic frogs, Xenopus laevis". 

Phoebe A. Chapman1, Daniel Hudson1, Xochitl C. Morgan2,3*, Caroline W. Beck1*. 

1Department of Zoology, University of Otago, Dunedin, New Zealand. 

2Department of Microbiology and Immunology, University of Otago, Dunedin, New Zealand. 

3Department of Biostatistics, Harvard T. H. Chan School of Public Health, Boston, Massachusetts, USA.  

\* equal contribution

This manuscript was submitted to Animal Microbiome on 6 Oct 2023.
Sequence data has been deposited in Sequence Read Archive (SRA) under BioProject ID PRJNA823390 (BioSamples SAMN33958523 - SAMN33958729)
